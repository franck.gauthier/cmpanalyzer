##########################################
######## Main initialization
################################################

rm(list=ls())

Input_folders=c("initialization_selfing_lowdiv")
source("R/CMP2CMPanalyseR.R")
source("R/CMPanalyseR.R")
source("R/presentation.R")
source("R/readCMdata.R")
source("R/init_sampling.R")
source("R/init_sampling.R")
source("R/readCMdata.R")


x=readCMPdata(folder="data", data.type= 1)


out.all = CMP2CMPanalyseR(x, n.sample =NULL, step =1 , parr = F ,final.generation = TRUE, nb_cor = 3)

out <- CMPanalyseR(out.all, stat= c("hs","fst") , pair.sample = 10,  plot = TRUE, save.rdata = TRUE, save.plot = T, bound = NULL, vs.go = FALSE,global.ne = T, parr = T)

nb_pop = 10
ls_nb_ind=c(10)
opt="equal_repeated"
initialization(x,nb_pop=1,ls_nb_ind=100,opt="equal_repeated", folder)
