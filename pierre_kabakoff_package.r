###kabakoff
## essai de création de package
# mettre les différentes fonction dans le format correct (voir dossier cropout)
#
install.packages("openssl", depend=TRUE)## pour le faire marché j'ai du passer par le terminal:  apt-cache policy libssl-dev

install.packages("roxygen2", depend=TRUE)

library(roxygen2)
library(openssl)

roxygenize("CropMetapopAnalyseR")




system("R CMD build CropMetapopAnalyseR")# crre le package de façon compressé
system("R CMD Rd2pdf CropMetapopAnalyseR")# cree le document pdf à partir des différents commentaires dans les formats requis (voir dossier cropout)

system("R CMD INSTALL CropMetapopAnalyseR") # installer le package locallement
library("CropMetapopAnalyseR")



install.packages(paste(getwd(),"/CropMetapopAnalyseR_1.0.tar.gz",sep=""),
                 repos=NULL, type="source")


?df2list
detach(package:cropout, unload=TRUE)## desinstaller le package pour des raison de modification des codes et autres
remove.packages("cropout")
######
#####
#####
##### Pierre
## création d'un package doc envoyé par Pierre
install.packages(c("devtools", "roxygen2", "testthat", "knitr"))

devtools::install_github("hadley/devtools")

library(devtools)
has_devel()


### les package suivant permet de formater les codes pour les rendre plus facile à lire:
install.packages("formatR")
formatR::tidy_dir("R")


## et aussi,
install.packages("lintr")
lintr::lint_package()

#ps: Variable and function names should be lowercase
#Utilisez un trait de soulignement (_) pour séparer les mots d'un nom 
#(réserve pour les méthodes S3). Camel case est une alternative légitime, 
#mais soyez cohérent! Généralement, les noms de variables doivent être des 
#noms et les noms de fonctions doivent être des verbes. Efforcez-vous de
#trouver des noms concis et significatifs (ce n'est pas facile!).
#attention changer le nom de la fonction each car elle correspond à une fonction existante aussi!!!!
