rm(list=ls())
rm(list=ls(all.names=TRUE))# different de précedent car libère les commençant par .
gc(verbose = T)## garbage collection libère la memoire
#setwd("~/Documents/depot git/output_genetic_indices/outputScript")
graphics.off()

stat =c("n.pop", "nb.pop", "occ.rate", "n.meta", "surv.rate", "nb.ind.out.col", "nb.ind.in.col", "nb.ind.col", "nb.event.in.col", "nb.event.out.col", "nb.event", "autoproduction.rate","hs", "hobs", "fis", "ar","mlh", "ht", "fst","gst", "d", "ne")

#setwd("~/Documents/cas_italie")
setwd("/home/kader/Documents/depot git/output_genetic_indices/outputScript")

x=readCMPdata(folder="data", data.type="mono")

out.all = CMP2CMPanalyseR(x, n.sample = 5, step = 5 , parr = F ,final.generation = TRUE, nb_cor = 3)

#out.all.2= out.all$col_seed_transfert

out <- CMPanalyseR(out.all, stat = "all" , pair.sample = 2,  plot = TRUE, save.rdata = T, save.plot = T, bound = NULL, vs.go = T,global.ne = T, parr = F,nb_cor = 3)


##pLOT

demo1=c("tom_highdiv_lowstress_indep")
demo2= c("tom_highdiv_lowstress_indep" , "tom_highdiv_lowstress_barabasi")



ls.file.all=list( demo1 ,demo2)
network.lty=c("barabasi:longdash","indep:solid")### changer en lty.
species.col=c("wheat:orange","maize:blue","cab:cyan","tom:red","italie:yellow")
other.pch=c("mono:1","multi:8")
#species.col=c("ital:orange")  ## ne marche pas si une seule couleur il faudrait charger tout
###################################################################################################################

ls.stats=c("survival","n.pop.mean","occupancy.mean")
ls.stats=c("survival","n.pop.mean","occupancy.mean","fst_wc","hs.mean","p.fst","ne")

plotCMPout(ls.file.all = ls.file.all, ls.caption = NULL ,ls.stats ,sel.opt = NULL,  lty.opt=network.lty,color.opt=species.col,pch.opt = other.pch)


## extraction of Climatic Data
setwd("~/Bureau/map_ital/mon.cas.24.10")
rm(list = ls())
library(raster)


samples <- data.frame(site = c("abruzzo","Sardinia","calabria","de Angeli","Bruni","sicile","san_marino","puglia","basilicata","Lombardia","piemonte","campania","lazzio","Emilia_romagna","veneto","molise","Tuscanny"), lon = c(13.7288991360058,9.01280481316806,16.3459120666267,11.8861809204656,11.1923085742033,14.0153665335325,12.4577265368116,17.101149288568,15.9698451868541,  9.84515024704326,7.51538753177025,14.8475019826924,12.9894864615604,11.2185517521681,11.6909327444506,14.7520433369393,11.248617872552), lat = c(42.1919862511421,40.120737038295,39.3077859292472,45.1972221292339,45.6251414924442,37.5999581081791,43.942168279469,40.7927280291258,40.6427714699017,45.4789213999343,45.0522293841684,41.109903461716,41.6549635774812,44.5964684994537,45.7622764722334,41.6738057126005,43.7710455337571))

precipitation = ReadWorlclimData(samples, folder = "wc2.0_30s_prec", write.data = T, data.name= "precipitation", GADM.code = "ITA")

## for all months
MapPlot (precipitation,value.range =NULL,rast.layer = NULL ,point.col="red",fig.name="" , save.plot=F, couleur = NULL)
## for months of interest
###precipitation
##April
MapPlot(precipitation,value.range =NULL,rast.layer = precipitation$country.data$Apr ,point.col="red",fig.name="Rain_April" , save.plot=T, couleur = NULL, point.name = TRUE)

##May
MapPlot (precipitation,value.range =NULL,rast.layer = precipitation$country.data$May ,point.col="red",fig.name="Rain_May" , save.plot=T, couleur = NULL, point.name = FALSE)

setwd("~/Documents/cas_italie/index_environnemental")
#This analysis is the Hill and Smith method and is very similar to
#dudi.mix function.   The differences are that dudi.hillsmith allow to use various row weights#, while dudi.mix deals with ordered variables. The principal components of this #analysis are centered and normed vectors maximizing the sum of squared correlation coefficients with quantitat
#Hill, M. O., and A. J. E. Smith. 1976. Principal component analysis of taxonomic data with multistate discrete characters. taxon 25 , 249-255.

x = read.csv("alldata", header = TRUE, sep = ",")
index_env = indexEnv(x)

write.csv2(index_env$index,file = "index")
