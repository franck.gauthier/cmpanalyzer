#
sum.param.simu <- function (ls.file.all,ls.stats)
{
ls.file. <- ls.file.all[[1]]
infile. <- paste(ls.file.[1], "/", ls.file.[1], "_output.RData", sep = "")
load(infile.)
param.simu <- out$param_simu
replicates <- param.simu$general.information$replicates
nb_pop <- param.simu$general.information$nb_pop
nb_generation <- param.simu$general.information$generations
generations.opt <- param.simu$generations.opt
generations.opt. <- c(1:length(generations.opt2))

ls.stat.verif <- match.arg(ls.stats, choices = c("nb.pop", "Nmeta","survival",  "n.pop.mean",  "occupancy.mean",  "nb.ind.col.mean",  "nb.ind.in.col.mean", "nb.ind.out.col.mean","nb.ind.out.col.var", "nb.event.mean", "nb.event.var"   ,"nb.event.in.col.mean" ,"nb.event.in.col.var","nb.event.out.col.mean"," nb.event.out.col.var" , "autoproduction.rate.mean"  , "autoproduction.rate.var","gst","fst_wc" ,"D_Jost","ht" ,"hs.mean","hobs.mean" ,"fis.mean" ,"ar.mean" ,"si.mean","mlg.mean","emlg.mean","mlh.mean"), several.ok = TRUE)

if (sum(ls.stats %in% ls.stat.verif) != length(ls.stats))
{
  "%!in%" <- function(x, y) !(x %in% y)
  b <- try(ls.stats[[which(ls.stats %!in%ls.stat.verif)]], silent = TRUE)
  if (class(b) == "try-error")
  {
    b <- try(ls.stats[ls.stats %!in% ls.stat.verif], silent = TRUE)
  }
  stop("the orthograph of the folowing stat is (are) not correct: ",b)
}

ls.stats.dem <- c("nb.pop", "Nmeta","survival",  "n.pop.mean",  "occupancy.mean",  "nb.ind.col.mean",  "nb.ind.in.col.mean", "nb.ind.out.col.mean","nb.ind.out.col.var", "nb.event.mean", "nb.event.var"   ,"nb.event.in.col.mean" ,"nb.event.in.col.var","nb.event.out.col.mean"," nb.event.out.col.var" , "autoproduction.rate.mean"  , "autoproduction.rate.var")  

stat.dem <- intersect(ls.stats.dem, ls.stats)

ls.stats.gen <- c("gst","fst_wc" ,"D_Jost","ht" ,"hs.mean","hobs.mean" ,"fis.mean" ,"ar.mean" ,"si.mean","mlg.mean","emlg.mean","mlh.mean")
stat.gen <- intersect(ls.stats.gen , ls.stats)

list(replicates=replicates,nb_pop=nb_pop,nb_generation=nb_generation, generations.opt=generations.opt,stat.dem =stat.dem, stat.gen =stat.gen )
}